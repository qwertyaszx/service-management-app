import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LayoutControlComponent } from 'src/app/layout-control/layout-control.component';

@Component({
  selector: 'app-jobs',
  templateUrl: './jobs.component.html',
  styleUrls: ['./jobs.component.scss']
})
export class JobsComponent implements OnInit {

  constructor(private _router: Router, private layoutComponent: LayoutControlComponent) { }

  ngOnInit(): void {
  }

  home() {
    this._router.navigateByUrl('');
  }

  openEditPane() {
    this.layoutComponent.openEditPane();
  }
}
