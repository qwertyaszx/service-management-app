import { FormBuilder, FormControl, FormGroup, Validators, AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

// Services
import { AuthService } from '../../services/auth/auth.service';
import { SnackBarService } from '../../services/messaging/snack-bar.service'
import { TokenService } from 'src/app/services/auth/token.service';

export class LoginData {
  public constructor(init?: Partial<LoginData>) {
        Object.assign(this, init);
    }
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  /**
   * An object representing the user for the login form
   */
  public user: any;

  loginForm: FormGroup;

  isLoginFailed: Boolean = false;
  isLoggedIn: Boolean = false;
 
  constructor(public _authService: AuthService, private _router: Router, private _formBuilder: FormBuilder, private _snackBarService: SnackBarService, private _tokenService: TokenService) {
    this.loginForm = this._formBuilder.group({
      username: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required, Validators.minLength(8)])
    })
  }
 
  ngOnInit() {
    this.user = {
      username: '',
      password: ''
    };
  }
 
  login(data: any) {
    this._authService.login(data).subscribe(
      data => {
        this._tokenService.saveToken(data.accessToken);
        this._tokenService.saveUser(data);
        this.isLoginFailed = false;
        this.isLoggedIn = true;
        this._router.navigateByUrl('');
      },
      err => {
        this._snackBarService.openSnackBar("Login Failed", 2000);
      }
    );
  }
}
