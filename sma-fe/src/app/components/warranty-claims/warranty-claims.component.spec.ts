import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WarrantyClaimsComponent } from './warranty-claims.component';

describe('WarrantyClaimsComponent', () => {
  let component: WarrantyClaimsComponent;
  let fixture: ComponentFixture<WarrantyClaimsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WarrantyClaimsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WarrantyClaimsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
