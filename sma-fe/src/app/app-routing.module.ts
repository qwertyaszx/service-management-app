import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthCanActivateGuard } from './guards';

import { LoginComponent } from './components/login/login.component';
import { LayoutControlComponent } from './layout-control/layout-control.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { JobsComponent } from './components/jobs/jobs.component';
import { WorkOrdersComponent } from './components/work-orders/work-orders.component';
import { WarrantyClaimsComponent } from './components/warranty-claims/warranty-claims.component';

export const AppRoutes: Routes = [
  { path: 'login', pathMatch: 'full', component: LoginComponent },
  { path: '', component: LayoutControlComponent, canActivate: [AuthCanActivateGuard],
    children: [
      { path: 'jobs', pathMatch: 'full', component: JobsComponent},
      { path: 'work-orders', pathMatch: 'full', component: WorkOrdersComponent},
      { path: 'warranty-claims', pathMatch: 'full', component: WarrantyClaimsComponent},
    ]},
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(AppRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
