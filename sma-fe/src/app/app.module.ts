import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MaterialModule } from "./modules/material-ui.module";
import { SimplebarAngularModule } from 'simplebar-angular';
import { MaterialFileInputModule } from 'ngx-material-file-input';
//import { ResizableModule } from 'angular-resizable-element';

// Routers
import { AppRoutingModule } from './app-routing.module';
import { LayoutControlRoutingModule } from './layout-control/layout-control-routing.module';

// Components
import { AppComponent } from './app.component';
import { LayoutControlComponent } from './layout-control/layout-control.component';
import { HeaderComponent } from './layout-control/header/header.component';
import { MenuComponent } from './layout-control/menu/menu.component';
import { SubMenuComponent } from './layout-control/sub-menu/sub-menu.component';
import { ContentPaneComponent } from './layout-control/content-pane/content-pane.component';
import { EditPaneComponent } from './layout-control/edit-pane/edit-pane.component';
import { LoginComponent } from './components/login/login.component';

// Services
import { CustomHttpInterceptorService } from './services/auth/custom-http-interceptor.service';
import { JobsComponent } from './components/jobs/jobs.component';
import { WorkOrdersComponent } from './components/work-orders/work-orders.component';
import { WarrantyClaimsComponent } from './components/warranty-claims/warranty-claims.component';
import { NotFoundComponent } from './components/not-found/not-found.component';

@NgModule({
  declarations: [
    AppComponent,
    LayoutControlComponent,
    HeaderComponent,
    MenuComponent,
    SubMenuComponent,
    ContentPaneComponent,
    LoginComponent,
    EditPaneComponent,
    JobsComponent,
    WorkOrdersComponent,
    WarrantyClaimsComponent,
    NotFoundComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    LayoutControlRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MaterialModule,
    SimplebarAngularModule,
    MaterialFileInputModule,
    //ResizableModule,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: CustomHttpInterceptorService, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
