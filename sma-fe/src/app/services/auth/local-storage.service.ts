import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  constructor() { }

  /**
   * Stores a key-value pair in local storage
   * @param key Identifier of key-value pair
   * @param value  Data stored in key-value pair
   */
  set(key: string, value: string) {
    localStorage.setItem(key, value);
  }
  
  /**
   * Returns the value from a key value pair in local storage
   * @param key Identifier for key-value pair
   * @returns Value from the key-value pair
   */
  get(key: string) : string {
    let item = localStorage.getItem(key)
    return item ? item : '';
  }
  
  /**
   * Removes the associated key-value pair from local storage
   * @param key Identifier for key-value pair
   */
  remove(key: string) {
    localStorage.removeItem(key);
  }
}
