import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpBackend } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
    
  // http options used for making API calls
  private httpOptions: any;

  private baseUrl = environment.baseUrl;

  // JWT access and refresh tokens
  public accessToken: string = '';
  public refreshToken: string = '';
  
  // Expiration dates for access and refresh tokens
  public accessTokenExpires: Date = new Date(0);
  public refreshTokenExpires: Date = new Date(0);
  
  // Logged in user
  public username: string = '';
  
  // error messages received from the login attempt
  public errors: any = [];

  private http: HttpClient
  
  constructor(private _handler: HttpBackend) {
    this.http = new HttpClient(_handler);
    this.httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    };

  }
   
  /**
   * Attempts to log the user in with the given user credentials. Successful logins will return an pair of JWT access and refresh tokens.
   * @param user User object containing username and password
   */
  public login(user: any): Observable<any>  {
    return this.http.post(this.baseUrl + '/api/token/obtain/', JSON.stringify(user), this.httpOptions)
  }

  register(username: string, email: string, password: string): Observable<any> {
    return this.http.post(this.baseUrl + '/api/token/register/', {
      username,
      email,
      password
    }, this.httpOptions);
  }
}
