import { Injectable } from '@angular/core';

import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class SnackBarService {

  constructor(private _snackBar: MatSnackBar) { }

  /**
   * Displays a snackbar message on the bottom centre of the screen for the given duration
   * @param message Message to be displayed on snackbar
   * @param durationVal Duration of snackbar in milliseconds
   */
  openSnackBar(message: string, durationVal: number) {
    this._snackBar.open(message, '', {duration: durationVal});
  }
}
