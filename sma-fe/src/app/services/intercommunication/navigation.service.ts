import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

export interface Breadcrumb {
  display: string;
  reference: string;
  referenceType: string;
}

@Injectable({
  providedIn: 'root'
})
export class NavigationService {

  currentPage: string = '';
  breadcrumb: Breadcrumb[] = [];
  breadcrumbSubject : Subject<Breadcrumb[]> = new Subject();
  currentPageSubject : Subject<string> = new Subject();
  homeBreadcrumb : Breadcrumb = { display: 'Home', reference: 'home', referenceType: 'router'}

  constructor() { 
  }

  
  initializeService() {
    this.setBreadcrumb([this.homeBreadcrumb]);
    this.setCurrentPage("Home");
  }

  setCurrentPage(currentPage: string) {
    this.currentPage = currentPage;
    this.currentPageSubject.next(currentPage);
  }

  getCurrentPage() : string {
    return this.currentPage;
  }

  setBreadcrumb(breadcrumb: Breadcrumb[]) {
    console.log(breadcrumb);
    this.breadcrumb = breadcrumb;
    this.breadcrumbSubject.next(this.breadcrumb);
  }

  addToBreadcrumb(disp: string, ref: string, refType: string) {
    this.breadcrumb.concat({
      display: disp,
      reference: ref,
      referenceType: refType
    })
  }

  getBreadcumb() : Breadcrumb[] {
   return this.breadcrumb;
  }
}
