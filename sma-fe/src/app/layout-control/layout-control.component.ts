import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-layout-control',
  templateUrl: './layout-control.component.html',
  styleUrls: ['./layout-control.component.scss']
})
export class LayoutControlComponent implements OnInit {

  root = document.documentElement;
  style: any;

  // Container Size Height Variables
  defaultBannerHeight = 0; // [px]
  minimumBannerHeight = 40; // [px]
  maximumBannerHeight = 40; // [px]
  bannerHeight: any;

  defaultHeaderHeight = 150; // [px]
  minimumHeaderHeight = 100; // [px]
  maximumHeaderHeight = 200; // [px]
  headerHeight: any;

  // Container Size Width Variables
  defaultMenuWidth = 70; // [px]
  minimumMenuWidth = 70; // [px]
  maximumMenuWidth = 70; // [px]
  menuWidth: any;

  defaultSubMenuWidth = 260; // [px]
  minimumSubMenuWidth = 0; // [px]
  maximumSubMenuWidth = 260; // [px]
  subMenuWidth: any;

  defaultActionMenuWidth = 0; // [px]
  minimumActionMenuWidth = 0; // [px]
  maximumActionMenuWidth = 70; // [px]
  actionMenuWidth: any;

  defaultEditPaneWidth = 500; // [px]
  minimumEditPaneWidth = 400; // [px]
  maximumEditPaneWidth = 9999; // [px]
  editPaneWidth: any;

  handlerWidth = 4;

  // Limit Variables
  windowWidth: any;
  leftContentContainerLimit: any;
  rightContentContainerLimit: any;

  // Component Open Booleans
  subMenuOpen = true;
  editPaneOpen = false;
  actionMenuOpen = false;

  // Draggable Dive Booleans
  isHandlerDragging = false;

  constructor() {
    window.addEventListener("resize", this.onWindowResize);
  }

  ngOnInit(): void {
    this.setDefaultContainerSizes();
    this.setAllContainerProperties();
    this.onWindowResize();
  }

  
  setDefaultContainerSizes() {
    // Heights
    this.headerHeight = this.defaultHeaderHeight;
    this.bannerHeight = this.defaultBannerHeight;

    // Widths
    this.menuWidth = this.defaultMenuWidth;
    this.subMenuWidth = this.defaultSubMenuWidth
    this.actionMenuWidth = this.defaultActionMenuWidth;
  }

  setAllContainerProperties() {
    this.setMenuProperties();
    this.setSubMenuProperties();
    this.setHeaderProperties();
    this.setEditPaneProperties();
    this.setActionMenuProperties();
  }

  onWindowResize() {
    console.log("window resize");
    this.windowWidth = window.innerWidth;
    this.setContentContainerLimits();
    this.setContentContainerProperties();
  }

  // Individual Container Property Setters

  setMenuProperties() {
    this.root.style.setProperty('--menu-width', this.menuWidth + 'px');
  }

  setSubMenuProperties() {
    this.root.style.setProperty('--sub-menu-width', this.subMenuWidth + 'px');
  }

  setContentContainerLimits() {
    this.leftContentContainerLimit = this.menuWidth + this.subMenuWidth;
    this.rightContentContainerLimit = this.windowWidth - this.actionMenuWidth;
    this.maximumEditPaneWidth = this.rightContentContainerLimit - this.leftContentContainerLimit;
  }

  setContentContainerProperties() {
    this.root.style.setProperty('--left-content-container-limit', this.leftContentContainerLimit + 'px');
    this.root.style.setProperty('--right-content-container-limit', this.rightContentContainerLimit + 'px');
  }

  setLeftContentContainerLimit() {
    this.root.style.setProperty('--left-content-container-limit', this.leftContentContainerLimit + 'px');
  }

  setRightContentContainerLimit() {
    this.root.style.setProperty('--right-content-container-limit', this.rightContentContainerLimit + 'px');
  }

  setHeaderProperties() {
    this.root.style.setProperty('--header-height', this.headerHeight + 'px');
  }

  setBannerProperties() {
    this.root.style.setProperty('--banner-height', this.bannerHeight + 'px');
  }

  setActionMenuProperties() {
    this.root.style.setProperty('--action-menu-width', this.actionMenuWidth + 'px');
  }

  setEditPaneProperties() {
    if (!this.editPaneOpen) {
      this.editPaneWidth = 0;
    }
    this.root.style.setProperty('--edit-pane-width', this.editPaneWidth + 'px');
    this.root.style.setProperty('--handler-width', this.handlerWidth + 'px');
  }

  openEditPane() {
    this.editPaneWidth = this.defaultEditPaneWidth;
    this.editPaneOpen = true;
    this.setEditPaneProperties();
  }

  // Draggable div functions
  resize(e: MouseEvent): void {
    //console.log(e);
    // Don't do anything if dragging flag is false
    if (!this.isHandlerDragging) {
      return;
    }
    // Get offset
    //const containerOffsetLeft = this.wrapper.nativeElement.offsetLeft;

    // Get x-coordinate of pointer relative to container
    let newEditPaneWidth = this.rightContentContainerLimit - e.clientX;


    if(newEditPaneWidth < this.minimumEditPaneWidth) {
      newEditPaneWidth = this.minimumEditPaneWidth;
    } else if(newEditPaneWidth > this.maximumEditPaneWidth){
      newEditPaneWidth = this.maximumEditPaneWidth;
    }
    this.editPaneWidth = newEditPaneWidth;

    this.setEditPaneProperties();
  }
}
