import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

// Services
import { Breadcrumb, NavigationService } from 'src/app/services/intercommunication/navigation.service';

// Components
import { LayoutControlComponent } from '../layout-control.component';

@Component({
  selector: 'app-sub-menu',
  templateUrl: './sub-menu.component.html',
  styleUrls: ['./sub-menu.component.scss']
})
export class SubMenuComponent implements OnInit {

  constructor(private layoutControlComponent: LayoutControlComponent, private _navigationService: NavigationService, private _router: Router) { }

  currentBreadcumb : Breadcrumb[] = [];
  homeBreadcrumb : Breadcrumb = { display: 'Home', reference: 'home', referenceType: 'router'}
  jobsBreadcrumb : Breadcrumb = { display: 'Jobs', reference: 'home', referenceType: 'router'}
  workOrdersBreadcrumb : Breadcrumb = { display: 'Work Orders', reference: 'home', referenceType: 'router'}
  warrantyClaimsBreadcrumb : Breadcrumb = { display: 'Warranty Claims', reference: 'home', referenceType: 'router'}

  ngOnInit(): void {
  }

  jobs() {
    this.currentBreadcumb = [this.homeBreadcrumb, this.jobsBreadcrumb];
    this._navigationService.setBreadcrumb(this.currentBreadcumb);
    this._navigationService.setCurrentPage('Jobs');
    this._router.navigateByUrl('jobs');
  }

  workOrders() {
    this.currentBreadcumb = [this.homeBreadcrumb, this.workOrdersBreadcrumb];
    this._navigationService.setBreadcrumb(this.currentBreadcumb);
    this._navigationService.setCurrentPage('Work Orders');
    this._router.navigateByUrl('work-orders');
  }

  warrantyClaims() {
    this.currentBreadcumb = [this.homeBreadcrumb, this.warrantyClaimsBreadcrumb];
    this._navigationService.setBreadcrumb(this.currentBreadcumb);
    this._navigationService.setCurrentPage('Warranty Claims');
    this._router.navigateByUrl('warranty-claims');
  }
}
