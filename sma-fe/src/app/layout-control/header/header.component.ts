import { Component, OnInit } from '@angular/core';
import { Breadcrumb, NavigationService } from 'src/app/services/intercommunication/navigation.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  breadcrumb: Breadcrumb[] = [];
  currentPage: string = '';

  constructor(private _navigationService : NavigationService) { }

  ngOnInit(): void {
    this._navigationService.breadcrumbSubject.subscribe((breadcrumb: Breadcrumb[])  => {
      this.breadcrumb = breadcrumb;
    });
    this._navigationService.currentPageSubject.subscribe((currentPage: string) => {
      this.currentPage = currentPage;
    });

    this._navigationService.initializeService();
  }

  updateBreadcrumb() {
  }


}
