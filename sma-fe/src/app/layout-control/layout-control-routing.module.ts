import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// Components
import { JobsComponent } from '../components/jobs/jobs.component';

export const LayoutControlRoutes: Routes = [
  { path: 'jobs', pathMatch: 'full', component: JobsComponent, outlet: 'content' },
  { path: 'work-orders', pathMatch: 'full', component: JobsComponent, outlet: 'content' },
  { path: 'warranty-claims', pathMatch: 'full', component: JobsComponent, outlet: 'content' },
];

@NgModule({
  imports: [RouterModule.forChild(LayoutControlRoutes)],
  exports: [RouterModule]
})
export class LayoutControlRoutingModule { }
