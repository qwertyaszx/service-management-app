import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LayoutControlRoutingModule } from './layout-control-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    LayoutControlRoutingModule
  ]
})
export class LayoutControlModule { }
